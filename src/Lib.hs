{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Lib where

import Data.Aeson (Value (Object, String, Null), (.=))
import qualified Data.Aeson as A
import Data.Csv (FromField (parseField))
import qualified Data.Map.Strict as M
import Protolude
import qualified Streaming.ByteString.Char8 as Q
import Streaming.Cassava ( decodeByName )
import qualified Streaming.Prelude as S

newtype V = V Value deriving (Show)

instance FromField V where
  parseField b = pure . V $ case A.decodeStrict b of
    Nothing -> case decodeUtf8' b of 
      Left _ue -> Null 
      Right txt -> String txt
    Just v -> v

makeObject :: Map Text V -> Value
makeObject m = Object $
  mconcat $ do
    (k, V v) <- M.assocs m
    pure $ k .= v

run :: IO ()
run = do
  r <-
    runExceptT
      . S.mapM_ putLByteString
      . S.map (A.encode . makeObject)
      . decodeByName
      $ Q.getContents
  case r of
    Left cpe -> panic $ show cpe
    Right x -> pure x
